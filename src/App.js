
import {gDevcampReact, calStudentPercent} from './info';
function App() {
  return (
    <div>
      <h1> {gDevcampReact.title} </h1>
      <img src={gDevcampReact.image} width='500px'/>
      <p>Tỷ lệ sinh viên đang theo học: {calStudentPercent()}%;</p>
      <p>{calStudentPercent() > 15 ? "Sinh viên đăng ký học nhiều" : "Sinh viên đăng ký học ít"}</p>
      <ul>
        {
          gDevcampReact.benefits.map((element, index) => <li key={index}>{element}</li> )
        }
      </ul>
    </div>
  );
}

export default App;
