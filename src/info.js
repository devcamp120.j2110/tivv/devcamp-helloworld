import image from './assets/images/logo-og.png'

export const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image: image,
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100
  }
  //Ham tinh ti le sinh vien dang theo hoc
export const calStudentPercent = () => gDevcampReact.studyingStudents/gDevcampReact.totalStudents*100;

